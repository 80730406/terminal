package apps

import (
  "reflect"
  "../core"
)

// Name -> Reflect Type map
var JsListenersMap map[string]reflect.Type
// Channel + ID -> Object map
var JsListeners = make(map[string]interface{})

func init() {
  JsListenersMap = map[string]reflect.Type {

    "CashDeviceListener": reflect.TypeOf((*CashDeviceListenerPrototype)(nil)).Elem(),
  }
}

type CashDeviceListenerPrototype struct {
  es interface{}
  id int
}

func (m *CashDeviceListenerPrototype) Create(es interface{}, id int) {
  m.es = es
  m.id = id
}

func (m *CashDeviceListenerPrototype) Bill(bill core.Bill) {
  Go2Js(m.es, m.id, "bill", bill)
}
