package apps

import (
	"../core"
	"encoding/json"
	"fmt"
	"github.com/antage/eventsource"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"
  "reflect"
  "os"
  "path/filepath"
  "path"
  "errors"
  "log"
)

func Go2Js(es interface{}, id int, method string, args ...interface{}) {
  //e := es.(eventsource.EventSource)

  for a := range args {
    log.Println("Go2Js", a)
  }
}

type Js struct {
	es map[string]eventsource.EventSource
  // list of allowed html apps
  htmlapps map[string]string
  htmlpath string
}

type RPCCore struct {
	Method string
	Name   string
}

func (m *Js) Js2Go(ss map[string]interface{}, value *json.RawMessage) (interface{}, error) {
  // js args map (name -> value)
  var r map[string]interface{}
	err := json.Unmarshal(*value, &r)
	if err != nil {
		return nil, err
	}

  // callect args to call go function
  var args []reflect.Value
  // method type of s object
  var f reflect.Value
  // go function type (with arguments) of method 'f'
  var ft reflect.Type
  // go service/device to call
  var s interface{}

  name,t := r["object"].(string)
  if !t {
		return nil, errors.New("'object' param not set")
  }

  s = ss[name]

  method, t := r["method"].(string)
  if !t {
		return nil, errors.New("'method' param not set")
  }

  es_id, t := r["es_id"].(string)
  if !t {
		return nil, errors.New("'es_id' param not set")
  }

  f = reflect.ValueOf(s).MethodByName(method)
  if f.IsNil() {
		return nil, errors.New("wrong object method:" + name + ", " + r["method"].(string))
  }
  ft = f.Type()

  for key, value := range r {
    // split java script 'param_name' into slice 
    kk := strings.Split(key, "_")
    switch kk[0] {
    case "param":
      // go function argument type
      var p reflect.Type
      // convert js argument (value) to go type (p)
      var pg reflect.Value

      p = ft.In(len(args))

      c, t := JsListenersMap[p.Name()]
      if t {
        l := reflect.New(c).Interface().(core.ListenerPrototype)
        var id int
        id = reflect.ValueOf(value).Convert(reflect.TypeOf(id)).Interface().(int)
        l.Create(m.es[es_id], id)
        pg = reflect.ValueOf(l)
      }else {
        pg = reflect.ValueOf(value).Convert(p)
      }
      args = append(args, pg)
    }
  }
  f.Call(args)

  // TODO process return parameters

	return "OK", nil
}

func (m *Js) visit(path string, f os.FileInfo, err error) error {
  p, err := filepath.Rel(m.htmlpath, path)
  if err != nil {
    return err
  }
  p = strings.ToLower(p)
  m.htmlapps[p] = path
  return nil
}

func (m *Js) Create() error {
  m.es = make(map[string]eventsource.EventSource)
  m.htmlapps = make(map[string]string)
  m.htmlpath = core.Res()
  files, err := ioutil.ReadDir(m.htmlpath)
  if err != nil {
    return err
  }
  for _, f := range files {
    name := strings.ToLower(f.Name())
    if f.IsDir() {
      err := filepath.Walk(path.Join(m.htmlpath, f.Name()), m.visit)
      if err != nil {
        return err
      }
      m.htmlapps[name] = path.Join(m.htmlpath, f.Name(), "index.html")
    }
    if strings.HasSuffix(name, ".html") {
      name := strings.TrimSuffix(name, ".html")
      m.htmlapps[name] = path.Join(m.htmlpath,f.Name())
    }
  }
  return nil
}

func (m *Js) Close() error {
  return nil
}

func (m *Js) Handler(w http.ResponseWriter, r *http.Request) {
	pp := strings.Split(r.URL.Path, "/")

  // /js/events
  //
  // pp[0] == ""
  // pp[1] == "js"
  // pp[2] == "events"
  
  pp = pp[2:]

	switch pp[0] {
	case "events":
    if len(pp) > 1 {
      id := pp[1]
      es,t := m.es[id]
      if !t {
  			fmt.Fprintln(w, "bad es channel id", id)
  			return
      }
  		es.ServeHTTP(w, r)
    } else {
      id, err := core.UUID()
      if err != nil {
  			fmt.Fprintln(w, err)
  			return
      }
      
      m.es[id] = eventsource.New(nil, nil)
    	go func() {
    		iid := 1
    		for {
    			m.es[id].SendEventMessage("tick", "tick-event", strconv.Itoa(iid))
    			iid++
    			time.Sleep(2 * time.Second)
    		}
    	}()

			fmt.Fprintln(w, id)
      // js unable to handle redirects :(
      //
      // http.Redirect(w, r, "/js/events/" + id, http.StatusMovedPermanently)
      return
    }
	case "rpc":
		body, _ := ioutil.ReadAll(r.Body)

		var mm map[string]*json.RawMessage
		err := json.Unmarshal(body, &mm)
		if err != nil {
			fmt.Fprintln(w, err)
			return
		}

		for key, value := range mm {
			switch key {
      case "system":
        // ignore
			case "core":
				var c RPCCore
				err = json.Unmarshal(*value, &c)
				if err != nil {
					fmt.Fprintln(w, err)
					return
				}
				switch c.Method {
				case "app":
					_, ok := core.Apps[c.Name]
					if ok {
						fmt.Fprintln(w, "OK")
						return
					} else {
						fmt.Fprintln(w, "App does not exist", c.Name)
						return
					}
				case "devices_uuid":
					d, ok := core.Devices[c.Name]
					if ok {
            t := strings.Trim(path.Ext(reflect.TypeOf(d).String()),".")
						fmt.Fprintln(w, "{ \"type\" : \"" + t + "\", \"uuid\" : \"" +c.Name+"\" }")
						return
					} else {
						fmt.Fprintln(w, "Device does not exist", c.Name)
						return
					}
				case "devices_type":
          dt, t := core.TypesMap[c.Name]
          if !t {
						fmt.Fprintln(w, "Device type does not exist", c.Name)
            return
          }
          var buf string
          for k, v := range core.Devices {
            t := reflect.TypeOf(v).Implements(dt)
            if t {
              buf += "\"" + k + "\","
            }
          }
          buf = strings.TrimRight(buf, ",")
					fmt.Fprintln(w, "["+buf+"]")
					return
				case "service":
					_, ok := core.Services[c.Name]
					if ok {
						fmt.Fprintln(w, "OK")
						return
					} else {
						fmt.Fprintln(w, "Service does not exist", c.Name)
						return
					}
        default:
  				fmt.Fprintln(w, "bad method:", c.Method)
          return
				}
			case "service":
        r, err := m.Js2Go(core.Services, value)
        if err != nil {
  				fmt.Fprintln(w, err)
          return
        }
				fmt.Fprintln(w, r)
        return
			case "device":
        r, err := m.Js2Go(core.Devices, value)
        if err != nil {
  				fmt.Fprintln(w, err)
          return
        }
				fmt.Fprintln(w, r)
        return
      default:
				fmt.Fprintln(w, "bad key:", key)
        return
			}
		}
	case "js.js":
		buf, err := ioutil.ReadFile(core.Res() + "/js.js")
		if err != nil {
			fmt.Fprintln(w, err)
			return
		}
		fmt.Fprintln(w, string(buf))
    return
	default:
    if val, ok := m.htmlapps[path.Join(pp...)]; ok {
      buf, err := ioutil.ReadFile(val)
  		if err != nil {
  			fmt.Fprintln(w, "bad js app", err)
  			return
  		}
  		fmt.Fprintln(w, string(buf[:]))
      return
    }else {
			fmt.Fprintln(w, "bad js app path", pp[0])
      return
    }
	}
}
