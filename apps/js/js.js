core = {
  es : null,

  es_id : null,

  es_init: function() {
    if (core.es_id != null) {
      return core.es_id
    }

    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
      if (request.readyState == 4 && request.status == 200) {
        core.es_id = request.responseText.trim();
      }
    }
    request.open("GET", "/js/events", false);
    request.send(null);

    core.es = new EventSource("/js/events/" + core.es_id);
    return core.es_id
  },
  
  rpc: function(packet) {
    var html;
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
      if (request.readyState == 4 && request.status == 200) {
        html = request.responseText;
      }
    }
    request.open("POST", "/js/rpc", false);
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    request.send(JSON.stringify(packet));
    
    try {
      if (html.trim() == "OK") {
        return true
      }
      return JSON.parse(html)
    } catch(err) {
      throw html
    }
  },

  app: function(name) {
    var packet = {
      core: {
        method: 'app',
        name: name
      }
    };

    return core.rpc(packet)
  },

  devices_uuid: function(name) {
    var packet = {
      core: {
        method: 'devices_uuid',
        name: name
      }
    };

    var r = core.rpc(packet)
    return devices[r.type](r.uuid)
  },

  devices_type: function(name) {
    var packet = {
      core: {
        method: 'devices_type',
        name: name
      }
    };

    return core.rpc(packet)
  },

  listener_next : 0,

  listener_map : {},

  listener: function(name) {
    var l = listeners[name]()
    l.id = core.listener_next++
    return l
  },

  service: function(name) {
    var packet = {
      core: {
        method: 'service',
        name: name
      }
    };

    core.rpc(packet)

    return services[name]
  }
}

var listeners = {
  "CashDeviceListener" : function() {
    return {
      bill : function(bill) {
      },
    }
  }
}

var apps = {
  "Js" : {
    js2Go : function(ss, value) {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Js",
          method: "Js2Go",
          param_ss: ss,
          param_value: value,
        }
      };

      return core.rpc(packet)
    },
    visit : function(path, f, err) {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Js",
          method: "visit",
          param_path: path,
          param_f: f,
          param_err: err,
        }
      };

      return core.rpc(packet)
    },
    create : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Js",
          method: "Create",
        }
      };

      return core.rpc(packet)
    },
    close : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Js",
          method: "Close",
        }
      };

      return core.rpc(packet)
    },
    handler : function(w, r) {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Js",
          method: "Handler",
          param_w: w,
          param_r: r,
        }
      };

      return core.rpc(packet)
    },
  },
  "Mobile" : {
    create : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Mobile",
          method: "Create",
        }
      };

      return core.rpc(packet)
    },
    close : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Mobile",
          method: "Close",
        }
      };

      return core.rpc(packet)
    },
    handler : function(w, r) {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Mobile",
          method: "Handler",
          param_w: w,
          param_r: r,
        }
      };

      return core.rpc(packet)
    },
  },
  "TestOpenGL" : {
    handler : function(w, r) {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "TestOpenGL",
          method: "Handler",
          param_w: w,
          param_r: r,
        }
      };

      return core.rpc(packet)
    },
    create : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "TestOpenGL",
          method: "Create",
        }
      };

      return core.rpc(packet)
    },
    close : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "TestOpenGL",
          method: "Close",
        }
      };

      return core.rpc(packet)
    },
  }
}

var services = {
  "Hardware" : {
    enumeratePorts : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Hardware",
          method: "EnumeratePorts",
        }
      };

      return core.rpc(packet)
    },
    probe : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Hardware",
          method: "Probe",
        }
      };

      return core.rpc(packet)
    },
    create : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Hardware",
          method: "Create",
        }
      };

      return core.rpc(packet)
    },
    run : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Hardware",
          method: "Run",
        }
      };

      return core.rpc(packet)
    },
    close : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Hardware",
          method: "Close",
        }
      };

      return core.rpc(packet)
    },
  },
  "Money" : {
    create : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Money",
          method: "Create",
        }
      };

      return core.rpc(packet)
    },
    close : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Money",
          method: "Close",
        }
      };

      return core.rpc(packet)
    },
    bill : function(bill) {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Money",
          method: "Bill",
          param_bill: bill,
        }
      };

      return core.rpc(packet)
    },
    run : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Money",
          method: "Run",
        }
      };

      return core.rpc(packet)
    },
    clear : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Money",
          method: "Clear",
        }
      };

      return core.rpc(packet)
    },
    getAmount : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Money",
          method: "GetAmount",
        }
      };

      return core.rpc(packet)
    },
  },
  "Qiwi" : {
    create : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Qiwi",
          method: "Create",
        }
      };

      return core.rpc(packet)
    },
    close : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Qiwi",
          method: "Close",
        }
      };

      return core.rpc(packet)
    },
    run : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Qiwi",
          method: "Run",
        }
      };

      return core.rpc(packet)
    },
    payment : function(provider, amount) {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Qiwi",
          method: "Payment",
          param_provider: provider,
          param_amount: amount,
        }
      };

      return core.rpc(packet)
    },
  },
  "Restart" : {
    run : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Restart",
          method: "Run",
        }
      };

      return core.rpc(packet)
    },
    create : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Restart",
          method: "Create",
        }
      };

      return core.rpc(packet)
    },
    close : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Restart",
          method: "Close",
        }
      };

      return core.rpc(packet)
    },
  },
  "Update" : {
    create : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Update",
          method: "Create",
        }
      };

      return core.rpc(packet)
    },
    run : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Update",
          method: "Run",
        }
      };

      return core.rpc(packet)
    },
    close : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Update",
          method: "Close",
        }
      };

      return core.rpc(packet)
    },
  },
  "Web" : {
    handler : function(w, r) {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Web",
          method: "Handler",
          param_w: w,
          param_r: r,
        }
      };

      return core.rpc(packet)
    },
    create : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Web",
          method: "Create",
        }
      };

      return core.rpc(packet)
    },
    run : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Web",
          method: "Run",
        }
      };

      return core.rpc(packet)
    },
    close : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Web",
          method: "Close",
        }
      };

      return core.rpc(packet)
    },
  }
}

var devices = {
  "BillValidatorCCNET" : function(uuid) {
    return {
      create : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "Create",
          }
        };
        return core.rpc(packet)
      },
      close : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "Close",
          }
        };
        return core.rpc(packet)
      },
      id : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "Id",
          }
        };
        return core.rpc(packet)
      },
      run : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "Run",
          }
        };
        return core.rpc(packet)
      },
      open : function(port) {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "Open",
          param_port: port,
          }
        };
        return core.rpc(packet)
      },
      addListener : function(l) {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "AddListener",
            param_l: l.id,
          }
        };
        core.listener_map[l.id] = l
        return core.rpc(packet)
      },
      removeListener : function(l) {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "RemoveListener",
            param_l: l.id,
          }
        };
        delete core.listener_map[l.id]
        return core.rpc(packet)
      },
      process : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "Process",
          }
        };
        return core.rpc(packet)
      },
      bill2Buf : function(bills) {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "Bill2Buf",
          param_bills: bills,
          }
        };
        return core.rpc(packet)
      },
      buf2Bill : function(buf) {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "Buf2Bill",
          param_buf: buf,
          }
        };
        return core.rpc(packet)
      },
      writeCmd : function(cmd) {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "WriteCmd",
          param_cmd: cmd,
          }
        };
        return core.rpc(packet)
      },
      writeData : function(cmd, data) {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "WriteData",
          param_cmd: cmd,
          param_data: data,
          }
        };
        return core.rpc(packet)
      },
      readData : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "ReadData",
          }
        };
        return core.rpc(packet)
      },
      readCmd : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "ReadCmd",
          }
        };
        return core.rpc(packet)
      },
      reset : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "Reset",
          }
        };
        return core.rpc(packet)
      },
      setSecurity : function(highsecuritybills) {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "SetSecurity",
          param_highsecuritybills: highsecuritybills,
          }
        };
        return core.rpc(packet)
      },
      powerRecovery : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "PowerRecovery",
          }
        };
        return core.rpc(packet)
      },
      enableBillTypesAll : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "EnableBillTypesAll",
          }
        };
        return core.rpc(packet)
      },
      enableBillTypes : function(enabled, escrow) {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "EnableBillTypes",
          param_enabled: enabled,
          param_escrow: escrow,
          }
        };
        return core.rpc(packet)
      },
      stack : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "Stack",
          }
        };
        return core.rpc(packet)
      },
      cassetteStatus : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "CassetteStatus",
          }
        };
        return core.rpc(packet)
      },
      return : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "Return",
          }
        };
        return core.rpc(packet)
      },
      dispense : function(bills) {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "Dispense",
          param_bills: bills,
          }
        };
        return core.rpc(packet)
      },
      unload : function(cassette, bills) {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "Unload",
          param_cassette: cassette,
          param_bills: bills,
          }
        };
        return core.rpc(packet)
      },
      setCassetteType : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "SetCassetteType",
          }
        };
        return core.rpc(packet)
      },
      setOptions : function(opts) {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "SetOptions",
          param_opts: opts,
          }
        };
        return core.rpc(packet)
      },
      emptyDispenser : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "EmptyDispenser",
          }
        };
        return core.rpc(packet)
      },
      poll : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "Poll",
          }
        };
        return core.rpc(packet)
      },
      getAck : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "GetAck",
          }
        };
        return core.rpc(packet)
      },
      getStatus : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "GetStatus",
          }
        };
        return core.rpc(packet)
      },
      getBillTable : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "GetBillTable",
          }
        };
        return core.rpc(packet)
      },
      getIdentification : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "GetIdentification",
          }
        };
        return core.rpc(packet)
      },
      getExtendedIdentification : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "GetExtendedIdentification",
          }
        };
        return core.rpc(packet)
      }
    }
  }
}
