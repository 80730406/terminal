package apps

import (
	"fmt"
	"html/template"
	"net/http"
	"os"
	"path"
  "../core"
)

type page struct {
	Title string
	Body  string
}

type Mobile struct {
}

func (m *Mobile) Create() error {
  return nil
}

func (m *Mobile) Close() error {
  return nil
}

func (m *Mobile) Handler(w http.ResponseWriter, r *http.Request) {
	p := &page{Title: "title", Body: "test"}

	_, ff := path.Split(r.URL.Path)

	// TODO read allowed file lists before read content

	file := core.Res() + "/mobile_" + ff + ".html"

	_, err := os.Stat(file)
	if err != nil {
		file = core.Res() + "/mobile.html"
	}

	t, err := template.ParseFiles(file)
	if err != nil {
		fmt.Fprintln(w, err)
		return
	}
	t.Execute(w, p)
}
