package core

import (
  "net/http"
)

//
// Core Type 1. Application type. All application run by 'Web' service and appear as simple http handlers.
//
type App interface {
  Create() error
  Handler(w http.ResponseWriter, r *http.Request)
  Close() error
}

//
// Core Type 2. Service can be resident in memory and do background work like retrying operation, storying data,
// working with hardware.
//
type Service interface {
  Create() error
  Run() error
  Close() error
}

//
// Core Type 3. Device, hardware communication class.
//
type Device interface {
  // unique device string, which prevent confusion between similar devices.
  // we wana prevent connecting EUR billvalidater on top of USD billvalidator. in case
  // if ports swaps.
  Id() string
  Create() error
  Run() error
  Close() error
}

//
// device which require a COM port (device path) to work.
//
type COMDevice interface {
  Open(port string) error
}

//
// Low level Cash events.
//
// All listeners interface must have a suffix 'Listener' to be propertly
// exported to javascript
//
type CashDeviceListener interface {
  Bill(bill Bill)
}

//
// Device which can receive billnotes/coins from customer.
//
// All listener methods must have 'add...' and 'remove...' prefixes to be propertly
// exported to javascript
//
type CashDevice interface {
  AddListener(m CashDeviceListener)
  RemoveListener(m CashDeviceListener)
}

//
// All generated Listeners will get Prototype suffix
// and have implemented ListenerPrototype interface
//
// es - page unique event channel, id - listener object id
//
type ListenerPrototype interface {
  Create(es interface{}, id int)
}

//
// Configs
//
// Device configs, where we store all configuration information for a device
//
type DeviceConfig struct {
  UUID string
  Id string
  Type string
}

//
// COM device config, keeps port path/name information
//
type COMDeviceConfig struct {
  DeviceConfig

  Port string
}
