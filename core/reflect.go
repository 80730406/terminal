package core

import "reflect"

func init() {
  TypesMap = map[string]reflect.Type {
    "App": reflect.TypeOf((*App)(nil)).Elem(),
    "Bill": reflect.TypeOf((*Bill)(nil)).Elem(),
    "COMDevice": reflect.TypeOf((*COMDevice)(nil)).Elem(),
    "COMDeviceConfig": reflect.TypeOf((*COMDeviceConfig)(nil)).Elem(),
    "CashDevice": reflect.TypeOf((*CashDevice)(nil)).Elem(),
    "CashDeviceListener": reflect.TypeOf((*CashDeviceListener)(nil)).Elem(),
    "Device": reflect.TypeOf((*Device)(nil)).Elem(),
    "DeviceConfig": reflect.TypeOf((*DeviceConfig)(nil)).Elem(),
    "ListenerPrototype": reflect.TypeOf((*ListenerPrototype)(nil)).Elem(),
    "Service": reflect.TypeOf((*Service)(nil)).Elem(),
  }
}
