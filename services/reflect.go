package services

import "reflect"
import "../core"

func init() {
  core.ServicesMap = map[string]reflect.Type {
    "Hardware": reflect.TypeOf((*Hardware)(nil)).Elem(),
    "Money": reflect.TypeOf((*Money)(nil)).Elem(),
    "Qiwi": reflect.TypeOf((*Qiwi)(nil)).Elem(),
    "Restart": reflect.TypeOf((*Restart)(nil)).Elem(),
    "Update": reflect.TypeOf((*Update)(nil)).Elem(),
    "Web": reflect.TypeOf((*Web)(nil)).Elem(),
  }
}
