package services

import (
  "../core"
)

type Money struct {
  Bills []core.Bill
}

func (m *Money) Create() error {
  err := core.Depends("Hardware")  
  if err != nil {
    return err
  }

  for _, d := range core.Devices {
    drv, t := d.(core.CashDevice)
    if !t {
      continue
    }
    drv.AddListener(m)
  }
  
	return nil
}

func (m *Money) Close() error {
  for _, d := range core.Devices {
    drv, t := d.(core.CashDevice)
    if !t {
      continue
    }
    drv.RemoveListener(m)
  }
	return nil
}

func (m *Money) Bill(bill core.Bill) {
  m.Bills = append(m.Bills, bill)
}

func (m *Money) Run() error {
  return nil
}

func (m *Money) Clear() {
  m.Bills = []core.Bill{}
}

func (m *Money) GetAmount() int {
  return 0
}