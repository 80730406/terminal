package services

import (
	"log"
)

type Qiwi struct {
}

func (i *Qiwi) Create() error {
	return nil
}

func (i *Qiwi) Close() error {
	return nil
}

func (m *Qiwi) Run() error {
  return nil
}

func (i *Qiwi) Payment(provider string, amount int) {
	log.Println("qiwi payment", provider, amount)
}
