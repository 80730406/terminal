package devices

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"github.com/tarm/serial"
	"time"
	unsafe "unsafe"
  "../core"
)

var CCNTCRC16Tbl [256]uint16 = [...]uint16{
	0x0000, 0x1189, 0x2312, 0x329b, 0x4624, 0x57ad, 0x6536, 0x74bf,
	0x8c48, 0x9dc1, 0xaf5a, 0xbed3, 0xca6c, 0xdbe5, 0xe97e, 0xf8f7,
	0x1081, 0x0108, 0x3393, 0x221a, 0x56a5, 0x472c, 0x75b7, 0x643e,
	0x9cc9, 0x8d40, 0xbfdb, 0xae52, 0xdaed, 0xcb64, 0xf9ff, 0xe876,
	0x2102, 0x308b, 0x0210, 0x1399, 0x6726, 0x76af, 0x4434, 0x55bd,
	0xad4a, 0xbcc3, 0x8e58, 0x9fd1, 0xeb6e, 0xfae7, 0xc87c, 0xd9f5,
	0x3183, 0x200a, 0x1291, 0x0318, 0x77a7, 0x662e, 0x54b5, 0x453c,
	0xbdcb, 0xac42, 0x9ed9, 0x8f50, 0xfbef, 0xea66, 0xd8fd, 0xc974,
	0x4204, 0x538d, 0x6116, 0x709f, 0x0420, 0x15a9, 0x2732, 0x36bb,
	0xce4c, 0xdfc5, 0xed5e, 0xfcd7, 0x8868, 0x99e1, 0xab7a, 0xbaf3,
	0x5285, 0x430c, 0x7197, 0x601e, 0x14a1, 0x0528, 0x37b3, 0x263a,
	0xdecd, 0xcf44, 0xfddf, 0xec56, 0x98e9, 0x8960, 0xbbfb, 0xaa72,
	0x6306, 0x728f, 0x4014, 0x519d, 0x2522, 0x34ab, 0x0630, 0x17b9,
	0xef4e, 0xfec7, 0xcc5c, 0xddd5, 0xa96a, 0xb8e3, 0x8a78, 0x9bf1,
	0x7387, 0x620e, 0x5095, 0x411c, 0x35a3, 0x242a, 0x16b1, 0x0738,
	0xffcf, 0xee46, 0xdcdd, 0xcd54, 0xb9eb, 0xa862, 0x9af9, 0x8b70,
	0x8408, 0x9581, 0xa71a, 0xb693, 0xc22c, 0xd3a5, 0xe13e, 0xf0b7,
	0x0840, 0x19c9, 0x2b52, 0x3adb, 0x4e64, 0x5fed, 0x6d76, 0x7cff,
	0x9489, 0x8500, 0xb79b, 0xa612, 0xd2ad, 0xc324, 0xf1bf, 0xe036,
	0x18c1, 0x0948, 0x3bd3, 0x2a5a, 0x5ee5, 0x4f6c, 0x7df7, 0x6c7e,
	0xa50a, 0xb483, 0x8618, 0x9791, 0xe32e, 0xf2a7, 0xc03c, 0xd1b5,
	0x2942, 0x38cb, 0x0a50, 0x1bd9, 0x6f66, 0x7eef, 0x4c74, 0x5dfd,
	0xb58b, 0xa402, 0x9699, 0x8710, 0xf3af, 0xe226, 0xd0bd, 0xc134,
	0x39c3, 0x284a, 0x1ad1, 0x0b58, 0x7fe7, 0x6e6e, 0x5cf5, 0x4d7c,
	0xc60c, 0xd785, 0xe51e, 0xf497, 0x8028, 0x91a1, 0xa33a, 0xb2b3,
	0x4a44, 0x5bcd, 0x6956, 0x78df, 0x0c60, 0x1de9, 0x2f72, 0x3efb,
	0xd68d, 0xc704, 0xf59f, 0xe416, 0x90a9, 0x8120, 0xb3bb, 0xa232,
	0x5ac5, 0x4b4c, 0x79d7, 0x685e, 0x1ce1, 0x0d68, 0x3ff3, 0x2e7a,
	0xe70e, 0xf687, 0xc41c, 0xd595, 0xa12a, 0xb0a3, 0x8238, 0x93b1,
	0x6b46, 0x7acf, 0x4854, 0x59dd, 0x2d62, 0x3ceb, 0x0e70, 0x1ff9,
	0xf78f, 0xe606, 0xd49d, 0xc514, 0xb1ab, 0xa022, 0x92b9, 0x8330,
	0x7bc7, 0x6a4e, 0x58d5, 0x495c, 0x3de3, 0x2c6a, 0x1ef1, 0x0f78,
}

func CR_GetCRC16CCNET(buf []byte) uint16 {
	var TmpCRC uint16

	TmpCRC = 0

	for i := 0; i < len(buf); i++ {
		TmpCRC = CCNTCRC16Tbl[(TmpCRC^uint16(buf[i]))&0xFF] ^ (TmpCRC >> 8)
	}

	return TmpCRC
}

//
// state
//

const STATE_INIT_1 = 0
const STATE_INIT_2 = 1
const STATE_POLL = 2
const STATE_ACCEPTING = 3

//
// poll status
//

const STATUS_POWER_UP = 0x10
const STATUS_POWER_UP_BILL = 0x11
const STATUS_POWER_UP_CHASSIS = 0x12
const STATUS_INITIALIZE = 0x13
const STATUS_IDLIN = 0x14
const STATUS_ACCEPTING = 0x15
const STATUS_STACKING = 0x17
const STATUS_RETURNING = 0x18
const STATUS_DISABLED = 0x19
const STATUS_HOLDING = 0x1a
const STATUS_BUSY = 0x1b
const STATUS_REJECTING = 0x1c
const STATUS_DISPENSING = 0x1d
const STATUS_UNLOADING = 0x1e
const STATUS_SETTING_TYPE_CASSETTE = 0x21
const STATUS_DISPENDED = 0x25
const STATUS_UNLOADED = 0x26
const STATUS_INVALID_BILL_NUMBER = 0x28
const STATUS_SET_CASSETTE_TYPE = 0x29
const STATUS_INVALID_COMMAND = 0x30
const STATUS_DROP_CASSETTE_FULL = 0x41
const STATUS_DROP_CASSETTE_REMOVED = 0x42
const STATUS_JAM_IN_ACCEPTOR = 0x43
const STATUS_JAM_IN_STACKER = 0x44
const STATUS_CHEATED = 0x45
const STATUS_BB_ERROR = 0x47
const STATUS_ESCROW = 0x80
const STATUS_STACKED = 0x81
const STATUS_RETURNED = 0x82

//
// base status
//

const STATUS_ACK = 0x00
const STATUS_NAK = 0xff
const STATUS_ILLEGAL = 0x30

//
// code
//

type BillValidatorCCNET struct {
  id string
	BillTable  map[int]core.Bill
	BillTableR map[core.Bill]int
	State      int
	Port       *serial.Port
}

//
// Device
//

func (m *BillValidatorCCNET) Create() error {
	return nil
}

func (m *BillValidatorCCNET) Close() error {
  if m.Port != nil {
    err := m.Port.Close()
    m.Port = nil
    m.id = ""
    return err
  }
  return nil
}

func (m *BillValidatorCCNET) Id() string {
  return m.id
}

func (m *BillValidatorCCNET) Run() error {
  for true {
    err := m.Process()
    if err != nil {
      return err
    }
    time.Sleep(100 * time.Millisecond)
  }
  return nil
}

//
// COMDevice
//

func (m *BillValidatorCCNET) Open(port string) error {
	var err error
	c := &serial.Config{Name: port, Baud: 19200, ReadTimeout: 2000 * time.Millisecond}
	m.Port, err = serial.OpenPort(c)
	if err != nil {
		return err
	}

  i, err := m.GetIdentification()
  if err != nil {
    return err
  }
  m.id = i.PartNumber + ":" + i.Serial

  return nil
}

//
// CashDevice
//

func (m *BillValidatorCCNET) AddListener(l core.CashDeviceListener) {
  
}

func (m *BillValidatorCCNET) RemoveListener(l core.CashDeviceListener) {
  
}

//
// Funcs
//

func (m *BillValidatorCCNET) Process() error {
	switch m.State {
	case STATE_INIT_1:
		s, err := m.Poll()
		if err != nil {
			return err
		}
		switch s {
		case STATUS_POWER_UP:
      err = m.Reset()
      if err != nil {
        return err
      }
			return nil
		case STATUS_DISABLED:
      m.BillTable, err = m.GetBillTable()
    	for k, v := range m.BillTable {
    		m.BillTableR[v] = k
    	}
      err = m.SetSecurity(nil)
      if err != nil {
        return err
      }
      m.State = STATE_INIT_2
			return nil
		}
	case STATE_INIT_2:
		s, err := m.Poll()
		if err != nil {
			return err
		}
		switch s {
		case STATUS_DISABLED:
      err = m.PowerRecovery()
  		if err != nil {
  			return err
  		}
      err = m.EnableBillTypesAll()
  		if err != nil {
  			return err
  		}
      // power recovery dispensing wait
    case STATUS_DISPENSING:
    }
    m.State = STATE_POLL
  case STATE_POLL:
		s, err := m.Poll()
		if err != nil {
			return err
		}
		switch s {
		case STATUS_ACCEPTING:
      m.State = STATE_ACCEPTING
    }
  case STATE_ACCEPTING:
		s, err := m.Poll()
		if err != nil {
			return err
		}
		switch s {
		case STATUS_ACCEPTING:
		case STATUS_ESCROW:
      err = m.Stack()
  		if err != nil {
  			return err
  		}
    case STATUS_STACKING:
    case STATUS_STACKED:
      m.State = STATE_POLL
    }
	}
	return nil
}

func (m *BillValidatorCCNET) Bill2Buf(Bills []core.Bill) []byte {
	buf := []byte{0, 0, 0}

	for _, bill := range Bills {
		i := m.BillTableR[bill]
		byteindex := uint(i / 8)
		revbyteindex := uint(len(buf)) - 1 - byteindex
		bitindex := uint(i) - byteindex*8

		buf[revbyteindex] |= (1 << bitindex)
	}

	return buf
}

func (m *BillValidatorCCNET) Buf2Bill(buf []byte) []core.Bill {
  var bills []core.Bill
  b := buf[:3]
  v := binary.BigEndian.Uint32(b)

	for i := 0 ; i<len(b) * 8 ; i++ {
		bill := m.BillTable[i]
    if(v&0x01 == 0x01) {
      bills = append(bills, bill)
    }
    v = v >> 1
	}

	return bills
}

//
// port
//

type cmd_packet struct {
	Sync byte
	// Adr
	//
	// 00H Forbidden
	// 01H Bill-to-Bill unit
	// 02H Coin Changer
	// 03H Bill Validator
	// 04H Card Reader
	Adr byte
	Lng byte
	Cmd byte
}

func (m *BillValidatorCCNET) WriteCmd(cmd byte) error {
	return m.WriteData(cmd, nil)
}

func (m *BillValidatorCCNET) WriteData(cmd byte, data []byte) error {
	var crc uint16
	p := cmd_packet{Sync: 0x02, Adr: 0x01, Cmd: cmd}
	p.Lng = byte(unsafe.Sizeof(p)) + byte(len(data)) + byte(unsafe.Sizeof(crc))

	net := &bytes.Buffer{}
	err := binary.Write(net, binary.BigEndian, p)
	if err != nil {
		return err
	}
	err = binary.Write(net, binary.BigEndian, data)
	if err != nil {
		return err
	}
	crc = CR_GetCRC16CCNET(net.Bytes())
	err = binary.Write(net, binary.BigEndian, crc)
	if err != nil {
		return err
	}
	m.Port.Write(net.Bytes())

	return nil
}

type data_packet struct {
	// Fixed: 0x02
	Sync byte
	// Adr
	//
	// 00H Forbidden
	// 01H Bill-to-Bill unit
	// 02H Coin Changer
	// 03H Bill Validator
	// 04H Card Reader
	Adr byte
	// Length HEADER + DATA + CRC
	Lng byte
}

func (m *BillValidatorCCNET) ReadData() ([]byte, error) {
	var crc uint16
	p := data_packet{}

	net := &bytes.Buffer{}

	for net.Len() < int(unsafe.Sizeof(p)) {
		buf := make([]byte, int(unsafe.Sizeof(p))-net.Len())
		n, err := m.Port.Read(buf)
		if err != nil {
			return nil, err
		}
		net.Write(buf[0:n])
	}

	err := binary.Read(net, binary.BigEndian, &p)
	if err != nil {
		return nil, err
	}

	for net.Len() < int(p.Lng) {
		buf := make([]byte, int(p.Lng)-net.Len())
		n, err := m.Port.Read(buf)
		if err != nil {
			return nil, err
		}
		net.Write(buf[0:n])
	}

	data := make([]byte, p.Lng)
	n, err := net.Read(data)
	if err != nil {
		return nil, err
	}
	if n != len(data) {
		return nil, errors.New("broken buffer object")
	}
	err = binary.Read(net, binary.BigEndian, &crc)
	if err != nil {
		return nil, err
	}
	crc_packet := CR_GetCRC16CCNET(net.Bytes()[0 : net.Len()-int(unsafe.Sizeof(crc))])
	if crc != crc_packet {
		return nil, errors.New("bad crc")
	}

	return data, nil
}

func (m *BillValidatorCCNET) ReadCmd() (cmd int, data []byte, err error) {
	buf, err := m.ReadData()
	if err != nil {
		return 0, nil, err
	}

	return int(buf[0]), buf[1:], nil
}

//
// commands
//

func (m *BillValidatorCCNET) Reset() error {
	err := m.WriteCmd(0x30)
	if err != nil {
		return err
	}
	return m.GetAck()
}

func (m *BillValidatorCCNET) SetSecurity(HighSecurityBills []core.Bill) error {
	buf := m.Bill2Buf(HighSecurityBills)

	err := m.WriteData(0x32, buf)
	if err != nil {
		return err
	}
	return m.GetAck()
}

func (m *BillValidatorCCNET) PowerRecovery() error {
	err := m.WriteCmd(0x66)
	if err != nil {
		return err
	}
  cmd, _, err := m.ReadCmd()
	if err != nil {
		return err
	}
  switch cmd {
    // ACK (NO POWER CUT DETECTED)
  case 0x00:
    // POWER CUT WHILE PACKING
  case 0x17:
    // POWER CUT WHILE DISPENSING
  case 0x3c:
    m.EmptyDispenser()
    // POWER CUT WHILE UNLOADING
  case 0x3d:
  }
  return nil
}

func (m *BillValidatorCCNET) EnableBillTypesAll() error {
	buf := []byte{0xff, 0xff, 0xff, 0xff, 0xff, 0xff}

	err := m.WriteData(0x34, buf)
	if err != nil {
		return err
	}
	return m.GetAck()
}

func (m *BillValidatorCCNET) EnableBillTypes(Enabled []core.Bill, Escrow []core.Bill) error {
	buf := append(m.Bill2Buf(Enabled), m.Bill2Buf(Escrow)...)

	err := m.WriteData(0x34, buf)
	if err != nil {
		return err
	}
	return m.GetAck()
}

func (m *BillValidatorCCNET) Stack() error {
	err := m.WriteCmd(0x35)
	if err != nil {
		return err
	}
	return m.GetAck()
}

func (m *BillValidatorCCNET) CassetteStatus() error {
	return nil
}

func (m *BillValidatorCCNET) Return() error {
	err := m.WriteCmd(0x36)
	if err != nil {
		return err
	}
	return m.GetAck()
}

func (m *BillValidatorCCNET) Dispense(Bills map[core.Bill]int) error {
	buf := [32]byte{0}

  i:=0
	for k, v := range Bills {
		b := m.BillTableR[k]
    buf[i] = byte(b)
    i++
    buf[i] = byte(v)
    i++
	}

	err := m.WriteCmd(0x3c)
	if err != nil {
		return err
	}
	return m.GetAck()
}

func (m *BillValidatorCCNET) Unload(Cassette int, Bills int) error {
	err := m.WriteData(0x3d, []byte{byte(Cassette), byte(Bills)})
	if err != nil {
		return err
	}
	return m.GetAck()
}

func (m *BillValidatorCCNET) SetCassetteType() error {
	return nil
}

const OPTION_LED_OFF = 1 << 7
const OPTION_HOLD_BILL = 1 << 6
const OPTION_CHECK_FOR_TAPE = 1 << 5
const OPTION_TURN_SWITCH = 1 << 4
const OPTION_TIMEOUT_CONTROL_BIT_1 = 1 << 3
const OPTION_TIMEOUT_CONTROL_BIT_2 = 1 << 2

func (m *BillValidatorCCNET) SetOptions(opts byte) error {
	err := m.WriteData(0x68, []byte{opts})
	if err != nil {
		return err
	}
	return m.GetAck()
}

func (m *BillValidatorCCNET) EmptyDispenser() error {
	err := m.WriteCmd(0x67)
	if err != nil {
		return err
	}
	return m.GetAck()
}

func (m *BillValidatorCCNET) Poll() (int, error) {
	err := m.WriteCmd(0x33)
	if err != nil {
		return 0, err
	}
	cmd, _, err := m.ReadCmd()
	if err != nil {
		return 0, err
	}
	err = m.WriteCmd(0x00)
	if err != nil {
		return 0, err
	}
	return cmd, nil
}

func (m *BillValidatorCCNET) GetAck() error {
	cmd, data, err := m.ReadCmd()
	if err != nil {
		return err
	}
	if cmd != 0x00 {
		return errors.New(fmt.Sprint("bad ack", cmd, data))
	}
	return nil
}

func (m *BillValidatorCCNET) GetStatus() (EnabledBills []core.Bill, HighSecurityBills []core.Bill, err error) {
	err = m.WriteCmd(0x31)
	if err != nil {
		return nil, nil, err
	}
	buf, err := m.ReadData()
	if err != nil {
		return nil, nil, err
	}

	return m.Buf2Bill(buf[0:3]), m.Buf2Bill(buf[3:6]), nil
}

func (m *BillValidatorCCNET) GetBillTable() (map[int]core.Bill, error) {
  bills := make(map[int]core.Bill)

	err := m.WriteCmd(0x41)
	if err != nil {
		return nil, err
	}
	buf, err := m.ReadData()
	if err != nil {
		return nil, err
	}

  const step = 5

  for i := 0; i < len(buf); i+=step {
    bill := buf[i:i+step]
    
    var b core.Bill

    digit := int(bill[0])
    code := string(bill[1:4])
    per := bill[4]
    persign := int(per >> 7)
    perdata := int(per & 0x7f)
    if persign == 1 {
      b.Value = digit * (10 ^ perdata) * 100
    } else {
      b.Value = digit / (10 ^ perdata) * 100
    }
    
    switch code {
    case "USA":
      b.Currency = core.USD
    default:
      return nil, errors.New("unknown currency code")
    }
    
    bills[i / step] = b
  }
  
  return bills, nil
}

type Identification struct {
	PartNumber string
	Serial     string
	Id         []byte
}

func (m *BillValidatorCCNET) GetIdentification() (Identification, error) {
	err := m.WriteCmd(0x37)
	if err != nil {
		return Identification{}, err
	}
	buf, err := m.ReadData()
	if err != nil {
		return Identification{}, err
	}
	return Identification{PartNumber: string(buf[0:15]), Serial: string(buf[15:27]), Id: buf[27:34]}, nil
}

type ExtendedIdentification struct {
	PartNumber                      string
	Serial                          string
	AssetNumber                     []byte
	BootVersionValidatorHead        string
	ProgramVersionValidatorHead     string
	BootVersionCPUBoard             string
	ProgramVersionCPUBoard          string
	BootVersionPackerBoard          string
	ProgramVersionPackerBoard       string
	BootVersionCassetteProcessor1   string
	BootVersionCassetteProcessor2   string
	BootVersionCassetteProcessor3   string
	ProgramVersionCassetteProcessor string
}

func (m *BillValidatorCCNET) GetExtendedIdentification() (ExtendedIdentification, error) {
	err := m.WriteCmd(0x3e)
	if err != nil {
		return ExtendedIdentification{}, err
	}
	buf, err := m.ReadData()
	if err != nil {
		return ExtendedIdentification{}, err
	}
	e := ExtendedIdentification{PartNumber: string(buf[0:15]), Serial: string(buf[15:27]), AssetNumber: buf[27:35],
		BootVersionValidatorHead: string(buf[35:41]), ProgramVersionValidatorHead: string(buf[41:61]), BootVersionCPUBoard: string(buf[61:67]),
		ProgramVersionCPUBoard: string(buf[67:73]), BootVersionPackerBoard: string(buf[73:79]), ProgramVersionPackerBoard: string(buf[79:85]),
		BootVersionCassetteProcessor1: string(buf[85:91]), BootVersionCassetteProcessor2: string(buf[91:97]),
		BootVersionCassetteProcessor3: string(buf[97:103]), ProgramVersionCassetteProcessor: string(buf[103:109])}
	return e, nil
}
