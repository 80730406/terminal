package devices

import "reflect"
import "../core"

func init() {
  core.DevicesMap = map[string]reflect.Type {
    "BillValidatorCCNET": reflect.TypeOf((*BillValidatorCCNET)(nil)).Elem(),
  }
}
