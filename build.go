package main

// +build ignore

import (
	"bytes"
	"errors"
	"go/ast"
	"go/parser"
	"go/token"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path"
	"strings"
	"text/template"
  "golang.org/x/tools/go/types"
  "sort"
)

func main() {
	err := exportDirs()
	if err != nil {
		panic(err)
	}

	err = resources("services/web", "apps/mobile", "apps/js")
	if err != nil {
		panic(err)
	}

	err = build()
	if err != nil {
		panic(err)
	}
}

func resources(ss ...string) error {
	for _, s := range ss {
		t := "./bin/" + s
		os.MkdirAll(t, 0755)
		args := []string{"-r", s + "/", t}
		err := runPrint("cp", args...)
		if err != nil {
			return err
		}
	}
	return nil
}

func build() error {
	args := []string{"build", "-o", "bin/terminal", "main.go"}
	return runPrint("go", args...)
}

func FirstToUpper(dir string) string {
	return strings.ToUpper(dir[0:1]) + dir[1:]
}

func FirstToLower(dir string) string {
	return strings.ToLower(dir[0:1]) + dir[1:]
}

func exportDirs() error {
	var core Core
  var err error

  core.Services, err = exportDir("services")
	if err != nil {
		return err
	}

  core.Devices, err = exportDir("devices")
	if err != nil {
		return err
	}

  core.Apps, err = exportDir("apps")
	if err != nil {
		return err
	}

  {
    dir := "core"
    
    var Types []Service

    Types, core.Listeners, err = parseCore(dir)
  	if err != nil {
  		return err
  	}

    sort.Sort(ServicesByName(Types))
    
    for l := range core.Listeners {
      for m := range core.Listeners[l].Methods {
        for a := range core.Listeners[l].Methods[m].Args {
          for _, t := range Types {
            if core.Listeners[l].Methods[m].Args[a].Type == t.Name {
              core.Listeners[l].Methods[m].Args[a].Type = "core." + core.Listeners[l].Methods[m].Args[a].Type
            }
          }
        }
      }
    }

    {
    	var buf bytes.Buffer
    	templ, err := template.New("PKG_REFLECT_CORE").Parse(PKG_REFLECT_CORE)
    	if err != nil {
    		return err
    	}
      err = templ.Execute(&buf, Package{Name: dir, ExportName: "Types", Services: Types})
    	if err != nil {
    		return err
    	}

    	name := dir + "/" + "reflect.go"

    	log.Println("writing:", name)

    	err = ioutil.WriteFile(name, buf.Bytes(), 0644)
    	if err != nil {
    		return err
    	}
    }

    {
    	var buf bytes.Buffer

    	templ, err := template.New("JS_REFLECT").Parse(JS_REFLECT)
    	if err != nil {
    		return err
    	}
      _, err = templ.New("JS_LISTENER_TYPE").Parse(JS_LISTENER_TYPE)
    	if err != nil {
    		return err
    	}
      templ2, err := templ.New("JS_LISTENER").Parse(JS_LISTENER)
    	if err != nil {
    		return err
    	}
      _, err = templ2.New("JS_METHOD").Parse(JS_METHOD)
    	if err != nil {
    		return err
    	}
    	err = templ.Execute(&buf, core.Listeners)
    	if err != nil {
    		return err
    	}

    	name := "apps/js_reflect.go"

    	log.Println("writing:", name)

    	err = ioutil.WriteFile(name, buf.Bytes(), 0644)
    	if err != nil {
    		return err
    	}
    }
  }

	templ, err := template.New("CORE").Parse(CORE)
	if err != nil {
		return err
	}
	templ2, err := templ.New("LISTENER").Parse(LISTENER)
	if err != nil {
		return err
	}
	_, err = templ2.New("LISTENER_METHOD").Parse(LISTENER_METHOD)
	if err != nil {
		return err
	}
	templ3, err := templ.New("SERVICE").Parse(SERVICE)
	if err != nil {
		return err
	}
	_, err = templ3.New("SERVICE_METHOD").Parse(SERVICE_METHOD)
	if err != nil {
		return err
	}
	templ4, err := templ.New("DEVICE").Parse(DEVICE)
	if err != nil {
		return err
	}
	_, err = templ4.New("DEVICE_METHOD").Parse(DEVICE_METHOD)
	if err != nil {
		return err
	}
  
  var buf bytes.Buffer
	err = templ.Execute(&buf, core)
	if err != nil {
		return err
	}  

	name := "apps/js/js.js"
	log.Println("writing:", name)
  ioutil.WriteFile(name, []byte(buf.String()), 0644)

  return nil
}

func exportDir(dir string) ([]Service, error) {
	types, err := parseDir(dir)
	if err != nil {
		return nil, err
	}

  sort.Sort(ServicesByName(types))

	var buf bytes.Buffer
	templ, err := template.New("PKG_REFLECT").Parse(PKG_REFLECT)
	if err != nil {
		return nil, err
	}
  err = templ.Execute(&buf, Package{Name: dir, ExportName: FirstToUpper(dir), Services: types})
	if err != nil {
		return nil, err
	}

	name := dir + "/reflect.go"
	log.Println("writing:", name)
	err = ioutil.WriteFile(name, buf.Bytes(), 0644)
	if err != nil {
		return nil, err
	}

	return types, nil
}

type Core struct {
  Services []Service
  Listeners []Service
  Apps []Service
  Devices []Service
}

type MethodArgs struct {
  // argument name bill
	Name string
  // argument type core.Bill
	Type string
  // this method is a listener
  Listener bool
  // add listener method
  ListenerAdd bool
  // remove listener method
  ListenerRemove bool
}

type Method struct {
  // parent service name
  Service string
  // arguments list
	Args    []MethodArgs
  // object to call. uuid for device. service name (ex:"web") for a service.
	Id string
  // go method name
	Method  string
  // js name
	JsMethod string
}

type Service struct {
  // service name "Web", "Update"
  Name string
  // methods list
  Methods []Method
}

type ServicesByName []Service

func (a ServicesByName) Len() int           { return len(a) }
func (a ServicesByName) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ServicesByName) Less(i, j int) bool { return a[i].Name < a[j].Name }

type Package struct {
  Name string
  ExportName string
  Services []Service
}

func parseDir(dir string) ([]Service, error) {
	var services []Service

	dirFile, err := os.Open(dir)
	if err != nil {
		return nil, err
	}
	defer dirFile.Close()
	info, err := dirFile.Stat()
	if err != nil {
		return nil, err
	}
	if !info.IsDir() {
		return nil, errors.New("Path is not a directory: " + dir)
	}

	pkgs, err := parser.ParseDir(token.NewFileSet(), dir, filter, 0)
	if err != nil {
		return nil, err
	}
	for _, pkg := range pkgs {
		for ffname, f := range pkg.Files {
      fnamebase := path.Base(ffname)
      // file name without ext
      fname := strings.TrimRight(fnamebase, path.Ext(ffname))
      for name, object := range f.Scope.Objects {
				if object.Kind == ast.Typ && ast.IsExported(name) && strings.HasPrefix(strings.ToLower(name), strings.ToLower(fname)) {
					service := Service{Name:name}

    			for d := range f.Decls {
            // var f *ast.FuncDecl
    				f, t := f.Decls[d].(*ast.FuncDecl)
    				if t {
              if f.Recv == nil {
                continue
              }

              // get "m *Type" string of member function
              that := ""
              for i := 0; i < f.Recv.NumFields();i++  {
                a := f.Recv.List[i].Type.(*ast.StarExpr)
                that = types.ExprString(a.X)
              }
              
              // if function is not member of structure skip it
              if that != name {
                continue
              }

              // function/method name
              jsname := FirstToLower(f.Name.String())

              // var ft ast.FuncType
    					ft := f.Type
    					var args []MethodArgs
    					for i := 0; i < ft.Params.NumFields(); i++ {
                // var p ast.Field
    						p := ft.Params.List[i]
                // argument name
                n := strings.ToLower(p.Names[0].Name)
                // argument type name
                t := types.ExprString(p.Type)
                // does type contain word 'listener'?
                l := strings.Contains(strings.ToLower(t), "listener")
                // check if it has 'add' prefix
                la := l && strings.Contains(strings.ToLower(jsname), "add")
                // check if it has 'remove' prefix
                lr := l && strings.Contains(strings.ToLower(jsname), "remove")
    						args = append(args, MethodArgs{Name: n, Type: t, Listener:l, ListenerAdd: la, ListenerRemove:lr})
    					}
              m := Method{Service:name, JsMethod: jsname, Id: name, Method: f.Name.String(), Args: args}
              service.Methods = append(service.Methods, m)
    				}
    			}
          services = append(services, service)
				}
			}
		}
	}

	{
		dirs, err := dirFile.Readdir(-1)
		if err != nil {
			return nil, err
		}
		for _, info := range dirs {
			if info.IsDir() {
				t, e := parseDir(path.Join(dir, info.Name()))
				if e != nil {
					return nil, e
				}
        services = append(services, t...)
			}
		}
	}

	return services, nil
}

func parseCore(dir string) (services []Service, listeners []Service, err error) {
	dirFile, err := os.Open(dir)
	if err != nil {
		return nil, nil, err
	}
	defer dirFile.Close()
	info, err := dirFile.Stat()
	if err != nil {
		return nil, nil, err
	}
	if !info.IsDir() {
		return nil, nil, errors.New("Path is not a directory: " + dir)
	}

	pkgs, err := parser.ParseDir(token.NewFileSet(), dir, filter, 0)
	if err != nil {
		return nil, nil, err
	}
	for _, pkg := range pkgs {
		for _, f := range pkg.Files {
      for name, object := range f.Scope.Objects {
				if object.Kind == ast.Typ && ast.IsExported(name) {
          services = append(services, Service{Name: name})
          if strings.HasSuffix(strings.ToLower(name), "listener") {
            service := Service{Name:name}

            ts, t := object.Decl.(*ast.TypeSpec)
            if !t {
              continue
            }
            i, t := ts.Type.(*ast.InterfaceType)
            if !t {
              continue
            }
            // var fl FieldList
            fl := i.Methods
          
            // var f Field
      			for _, f := range fl.List {
              // function/method name
              jsname := FirstToLower(f.Names[0].Name)
            
    					ft := f.Type.(*ast.FuncType)
    					var args []MethodArgs
    					for i := 0; i < ft.Params.NumFields(); i++ {
    						p := ft.Params.List[i]
                // argument name
                n := strings.ToLower(p.Names[0].Name)
                // argument type name
                t := types.ExprString(p.Type)
    						args = append(args, MethodArgs{Name: n, Type: t})
    					}
              m := Method{Service: name, JsMethod: jsname, Id: name, Method: f.Names[0].Name, Args: args}
              service.Methods = append(service.Methods, m)
      			}
            listeners = append(listeners, service)
  				}
        }
			}
		}
	}

	{
		dirs, err := dirFile.Readdir(-1)
		if err != nil {
			return nil, nil, err
		}
		for _, info := range dirs {
			if info.IsDir() {
				t, tt, err := parseCore(path.Join(dir, info.Name()))
				if err != nil {
					return nil, nil, err
				}
        services = append(services, t...)
        listeners = append(listeners, tt...)
			}
		}
	}

	return services, listeners, nil
}

func filter(info os.FileInfo) bool {
	name := info.Name()
	return !info.IsDir() && path.Ext(name) == ".go" && !strings.HasSuffix(name, "_test.go")
}

func runPrint(cmd string, args ...string) error {
	log.Println(cmd, strings.Join(args, " "))
	ecmd := exec.Command(cmd, args...)
	ecmd.Stdout = os.Stdout
	ecmd.Stderr = os.Stderr
	err := ecmd.Run()
	return err
}

//
// CORE JS
//
var CORE = `core = {
  es : null,

  es_id : null,

  es_init: function() {
    if (core.es_id != null) {
      return core.es_id
    }

    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
      if (request.readyState == 4 && request.status == 200) {
        core.es_id = request.responseText.trim();
      }
    }
    request.open("GET", "/js/events", false);
    request.send(null);

    core.es = new EventSource("/js/events/" + core.es_id);
    return core.es_id
  },
  
  rpc: function(packet) {
    var html;
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
      if (request.readyState == 4 && request.status == 200) {
        html = request.responseText;
      }
    }
    request.open("POST", "/js/rpc", false);
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    request.send(JSON.stringify(packet));
    
    try {
      if (html.trim() == "OK") {
        return true
      }
      return JSON.parse(html)
    } catch(err) {
      throw html
    }
  },

  app: function(name) {
    var packet = {
      core: {
        method: 'app',
        name: name
      }
    };

    return core.rpc(packet)
  },

  devices_uuid: function(name) {
    var packet = {
      core: {
        method: 'devices_uuid',
        name: name
      }
    };

    var r = core.rpc(packet)
    return devices[r.type](r.uuid)
  },

  devices_type: function(name) {
    var packet = {
      core: {
        method: 'devices_type',
        name: name
      }
    };

    return core.rpc(packet)
  },

  listener_next : 0,

  listener_map : {},

  listener: function(name) {
    var l = listeners[name]()
    l.id = core.listener_next++
    return l
  },

  service: function(name) {
    var packet = {
      core: {
        method: 'service',
        name: name
      }
    };

    core.rpc(packet)

    return services[name]
  }
}

var listeners = {
{{range $i, $v := .Listeners}}{{if $i}},
{{end}}{{template "LISTENER" .}}{{end}}
}

var apps = {
{{range $i, $v := .Apps}}{{if $i}},
{{end}}{{template "SERVICE" .}}{{end}}
}

var services = {
{{range $i, $v := .Services}}{{if $i}},
{{end}}{{template "SERVICE" .}}{{end}}
}

var devices = {
{{range $i, $v := .Devices}}{{if $i}},
{{end}}{{template "DEVICE" .}}{{end}}
}
`

var SERVICE = `  "{{.Name}}" : {
{{range .Methods}}{{template "SERVICE_METHOD" .}}{{end}}  }`

var SERVICE_METHOD = `    {{.JsMethod}} : function({{range $i, $v := .Args}}{{if $i}}, {{end}}{{.Name}}{{end}}) {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "{{.Id}}",
          method: "{{.Method}}",
{{range $i, $v := .Args}}{{if .Listener}}          param_{{.Name}}: {{.Name}}.id,{{else}}          param_{{.Name}}: {{.Name}},{{end}}
{{end}}        }
      };
{{range $i, $v := .Args}}{{if .ListenerAdd}}      core.listener_map[{{.Name}}.id] = {{.Name}}
{{end}}{{if .ListenerRemove}}      delete core.listener_map[{{.Name}}.id]
{{end}}{{end}}
      return core.rpc(packet)
    },
`

var DEVICE = `  "{{.Name}}" : function(uuid) {
    return {
{{range $i, $v := .Methods}}{{if $i}},
{{end}}{{template "DEVICE_METHOD" .}}{{end}}
    }
  }`

var DEVICE_METHOD = `      {{.JsMethod}} : function({{range $i, $v := .Args}}{{if $i}}, {{end}}{{.Name}}{{end}}) {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "{{.Method}}",
{{range $i, $v := .Args}}{{if .Listener}}            param_{{.Name}}: {{.Name}}.id,{{else}}          param_{{.Name}}: {{.Name}},{{end}}
{{end}}          }
        };
{{range $i, $v := .Args}}{{if .ListenerAdd}}        core.listener_map[{{.Name}}.id] = {{.Name}}
{{end}}{{if .ListenerRemove}}        delete core.listener_map[{{.Name}}.id]
{{end}}{{end}}        return core.rpc(packet)
      }`

var LISTENER = `  "{{.Name}}" : function() {
    return {
{{range .Methods}}{{template "LISTENER_METHOD" .}}{{end}}    }
  }`

var LISTENER_METHOD = `      {{.JsMethod}} : function({{range $i, $v := .Args}}{{if $i}}, {{end}}{{.Name}}{{end}}) {
      },
`

//
// JS reflect classes
//

var JS_REFLECT = `package apps

import (
  "reflect"
  "../core"
)

// Name -> Reflect Type map
var JsListenersMap map[string]reflect.Type
// Channel + ID -> Object map
var JsListeners = make(map[string]interface{})

func init() {
  JsListenersMap = map[string]reflect.Type {
{{range .}}{{template "JS_LISTENER_TYPE" .}}{{end}}
  }
}
{{range .}}{{template "JS_LISTENER" .}}{{end}}`

var JS_LISTENER_TYPE = `
    "{{.Name}}": reflect.TypeOf((*{{.Name}}Prototype)(nil)).Elem(),`

var JS_LISTENER = `
type {{.Name}}Prototype struct {
  es interface{}
  id int
}

func (m *{{.Name}}Prototype) Create(es interface{}, id int) {
  m.es = es
  m.id = id
}
{{range .Methods}}{{template "JS_METHOD" .}}{{end}}`

var JS_METHOD = `
func (m *{{.Service}}Prototype) {{.Method}}({{range $i, $v := .Args}}{{if $i}}, {{end}}{{.Name}} {{.Type}}{{end}}) {
  Go2Js(m.es, m.id, "{{.JsMethod}}", {{range $i, $v := .Args}}{{if $i}}, {{end}}{{.Name}}{{end}})
}
`

//
// pkg reflect
//
var PKG_REFLECT = `package {{.Name}}

import "reflect"
import "../core"

func init() {
  core.{{.ExportName}}Map = map[string]reflect.Type {
{{range .Services}}    "{{.Name}}": reflect.TypeOf((*{{.Name}})(nil)).Elem(),
{{end}}  }
}
`

var PKG_REFLECT_CORE = `package {{.Name}}

import "reflect"

func init() {
  {{.ExportName}}Map = map[string]reflect.Type {
{{range .Services}}    "{{.Name}}": reflect.TypeOf((*{{.Name}})(nil)).Elem(),
{{end}}  }
}
`
