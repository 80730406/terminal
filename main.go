package main

import (
	"./core"

	"flag"
  
	_ "./apps"
	_ "./services"
	"./devices"
)

var WAIT = flag.Int("wait", 0, "wait for process to exit, before start")

func main() {
	flag.Parse()

	err := core.Create()
  if err != nil {
    core.Err(err)
    return
  }

  core.Devices["000-000"] = &devices.BillValidatorCCNET{}
  core.Devices["000-001"] = &devices.BillValidatorCCNET{}

	err = core.Run()
  if err != nil {
    core.Err(err)
    return
  }
  
	err = core.Close()
  if err != nil {
    core.Err(err)
    return
  }
}
